import argparse
from collections import namedtuple
import json
import math
import os
from pathlib import Path
from typing import Dict, Iterable, List, Set, Tuple


# Folders
scenarios_folder = Path(__file__).parents[1] / 'scenarios'

# Types
Account = str
Settings = namedtuple('Settings',
        [
            'start_time',
            'end_time'
        ]
    )
Scenario = namedtuple(
        'Scenario',
        [
            'events',
            'settings'
        ]
    )
Token = namedtuple(
        'Token',
        [
            'owner',
            'variable_power',
            'asymptotic_power',
            'mint_time'
        ]
    )
TokenType = namedtuple(
        'TokenType',
        [
            'name',
            'compute_power_of_token',
            'percentage_asymptotic_power'
        ]
    )


def get_exponential_decay_power_function(half_life: float):
    coef = math.log(2) / half_life
    def exponential_decay_function(token: Token, t: int) -> float:
        if t >= token.mint_time:
            delta_t = t - token.mint_time
            return token.variable_power * math.exp(- coef * delta_t) + token.asymptotic_power
        return 0

    return exponential_decay_function


def get_linear_decay_power_function_with_constant_slope(slope):
    def linear_decay(token: Token, t: int) -> float:
        if t < token.mint_time:
            return 0
        return max(0, - slope * (t - token.mint_time) + token.variable_power) + token.asymptotic_power

    return linear_decay


def get_linear_decay_power_function_with_constant_dt(dt):
    def linear_decay(token: Token, t: int) -> float:
        if t < token.mint_time:
            return 0
        return max(0, - token.variable_power / dt * (t - token.mint_time) + token.variable_power) + token.asymptotic_power

    return linear_decay


token_types: List[TokenType] = [
    TokenType('exponential_decay_30_0', get_exponential_decay_power_function(30), 0),
    TokenType('exponential_decay_30_50', get_exponential_decay_power_function(30), 0.5),
    TokenType('linear_decay_constant_slope', get_linear_decay_power_function_with_constant_slope(100/60), 0.5),
    TokenType('linear_decay_constant_dt', get_linear_decay_power_function_with_constant_dt(60), 0.5),
]


def load_scenario(path: Path) -> Scenario:
    '''
    Loads a scenario from the folder containing it.
    '''
    events = json.load(open(path / 'events.json'))
    settings: Settings = Settings(**json.load(open(path / 'settings.json')))

    return Scenario(events, settings)


def run_simulation(scenario: Scenario, token_type: TokenType) -> Tuple[Set[Account], Dict[int, Dict[Account, float]]]:
    '''
    Runs a simulation of a scenario.
    :param scenario: The scenario to simulate.
    :param compute_token_power: The function that returns the power of a token at a given moment in time.
    :returns: A set of all accounts that took part to the simulation, a mapping time step -> account -> total absolute power of account at time step.
    '''
    events_of_time_step: Dict[int, List[dict]] = {}
    '''Mapping from time step to all events that happen at the time step. If there are no events occurring, then the mapping will not contain the given time step.'''
    for event in scenario.events:
        if event['time'] not in events_of_time_step:
            events_of_time_step[event['time']] = []
        events_of_time_step[event['time']].append(event)

    accounts: Set[Account] = set()

    powers: Dict[int, Dict[Account, float]] = {}
    '''
    For each time step in the simulation, contains a dict from account to the shares owned by this account.
    The dict is guaranteed to contain an entry for each account in the simulation.
    '''

    existing_tokens: List[Token] = []
    '''The list of all tokens that exist so far.'''

    for time_step in range(scenario.settings.start_time, scenario.settings.end_time):
        # Process all the events of the time step.
        if time_step in events_of_time_step:
            for event in events_of_time_step[time_step]:
                if event['type'] == 'mint':
                    accounts.add(event['to'])
                    asymptotic_power = event['power'] * token_type.percentage_asymptotic_power
                    variable_power = event['power'] * (1.0 - token_type.percentage_asymptotic_power)
                    existing_tokens.append(Token(event['to'], variable_power, asymptotic_power, event['time']))

        # Compute the power that each account owns.
        powers_at_time_step = {}
        for token in existing_tokens:
            if token.owner not in powers_at_time_step:
                powers_at_time_step[token.owner] = 0
            powers_at_time_step[token.owner] += token_type.compute_power_of_token(token, time_step)
        powers[time_step] = powers_at_time_step

    return accounts, powers


def compute_absolute_powers_per_account(
        powers: Dict[int, Dict[Account, float]]
    ) -> Dict[Account, List[Tuple[int, float]]]:
    '''
    :returns: A mapping from account to a list of tuples, one tuple per time step in the simulation, each tuple containing a time index and the absolute power of the user at this time step.
    '''
    absolute_powers_per_account = {}
    for time_step, powers_at_time_step in powers.items():
        for account, power in powers_at_time_step.items():
            if account not in absolute_powers_per_account:
                absolute_powers_per_account[account] = []
            absolute_powers_per_account[account].append((time_step, power))

    return absolute_powers_per_account


def compute_relative_powers_per_account(
        accounts: Set[Account],
        powers: Dict[int, Dict[Account, float]]
    ) -> Dict[Account, List[Tuple[int, float]]]:
    '''
    :returns: A mapping from account to a list of tuples, one tuple per time step in the simulation, each tuple containing a time index and the relative power of the user at this time step.
    '''
    relative_powers_per_account = {}
    for time_step, powers_at_time_step in powers.items():
        total_power = sum(powers_at_time_step.values())
        for account in accounts:
            if account not in relative_powers_per_account:
                relative_powers_per_account[account] = []
            if account not in powers_at_time_step:
                relative_powers_per_account[account].append((time_step, 0))
            else:
                relative_powers_per_account[account].append((time_step, powers_at_time_step[account] / total_power))

    return relative_powers_per_account


def output_to_dat_files(
        data: Dict[Account, List[Tuple[int, float]]],
        output_folder: Path,
    ):
    output_folder.mkdir(parents=True, exist_ok=True)
    for account, d in data.items():
        with open(output_folder / f'{account}.dat', 'w') as out:
            out.writelines([f'{time_step} {value}\n' for time_step, value in d])


def dir_path(string: str) -> Path:
    path = Path(string)
    if path.is_dir():
        return path
    else:
        raise NotADirectoryError(string)


def execute_scenario(scenario_name: str, output_path: Path):
    scenario = load_scenario(scenarios_folder / scenario_name)

    for token_type in token_types:
        accounts, powers = run_simulation(scenario, token_type)
        absolute_powers_per_account = compute_absolute_powers_per_account(powers)
        relative_powers_per_account = compute_relative_powers_per_account(accounts, powers)
        output_to_dat_files(absolute_powers_per_account, output_path / scenario_name / token_type.name / 'absolute')
        output_to_dat_files(relative_powers_per_account, output_path / scenario_name / token_type.name / 'relative')


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('output_path', type=dir_path)
    args = parser.parse_args()

    execute_scenario('identical_power_different_time_steps', args.output_path)
    execute_scenario('different_powers_identical_time_step', args.output_path)
    execute_scenario('less_power_later', args.output_path)
    execute_scenario('main_contributor', args.output_path)

