# Blockchain Primitives for Trustless Open Source

This is my master thesis.
If you find it interesting, don't hesitate to star the repo or send me an email.

[Click here to access the full thesis](https://gitlab.com/yves_zumbach/blockchain-primitives-for-trustless-open-source/-/raw/main/master_thesis_yves_zumbach.pdf)

## Abstract

Open source software has become a crucial infrastructure in contemporary societies which is why it is important to protect it.
This work aims to design blockchain primitives that make open source **trustless**.

We explore a key element of trustlessness: **decentralization**; why we care about it, and how to achieve it.
The main proposal of this work is a voting system based on **tokens with time-decreasing power**, which opens a new design space for voting systems, including creating incentives for recurrent contributions and lowering power entrenchment.
Additionally, we propose a **rewarding scheme** to define the power of the tokens that should be awarded for contributions to an open source project, as well as a **strategy to distribute the money** received by the project to its contributors using the same tokens, aligning value creation and value extraction.
We propose a **voting workflow** specifically targeted at merge requests that improves security, by providing a challenge mechanism that deters adversarial proposals.
Finally, we propose a scheme to **back issues** with money for improved community feedback.

All these features build a coherent specification, called **GitDAO**, that improves the guarantees provided by any open source project that uses it.

## Screenshots

![Title page](./screenshots/screenshot_001.png)

![Abstract](./screenshots/screenshot_002.png)

![Table of content](./screenshots/screenshot_003.png)

![First page](./screenshots/screenshot_004.png)

